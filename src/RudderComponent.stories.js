import React from 'react';

import MainEngineComponent from "./MainEngineComponent";
import RudderComponent from "./RudderComponent";
import PropTypes from "prop-types";

export default {
    title: 'Rudder Component',
    component: RudderComponent,
};

const Template = (args) => <RudderComponent {...args} />;

export const Large = Template.bind({});
Large.args = {
    rudder_angle_degrees: 5,
    requested_rudder_angle_degrees: 10,
    maxAngleDegrees: 20,
    sml: 'l',
};
