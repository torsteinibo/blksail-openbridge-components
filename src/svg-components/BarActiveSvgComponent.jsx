import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class BarActiveSvgComponent extends Component {
    render() {
        const { x, y, bar_width, bar_height, sml } = this.props;
        let viewbox = '0 0 ' + bar_width + ' ' + bar_height;
        if (sml === 's') {
            return (
                <svg
                    x={x}
                    y={y}
                    width={bar_width}
                    height={bar_height}
                    viewBox={viewbox}
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M0 32H8V0.152663C6.68737 0.0518167 5.35263 0 3.99992 0C2.64727 0 1.31258 0.0518132 0 0.152651V32Z"
                        className="blksail-dynamic"
                    />
                </svg>
            );
        } else if (sml === 'm') {
            return (
                <svg
                    x={x}
                    y={y}
                    width={bar_width}
                    height={bar_height}
                    viewBox={viewbox}
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M2.72203e-07 97L24 97L24 0.935706C20.0622 0.647885 16.058 0.499999 12 0.499999C7.94193 0.499999 3.93781 0.647884 1.73138e-09 0.935702L2.72203e-07 97Z"
                        className="blksail-dynamic"
                    />
                </svg>
            );
        } else if (sml === 'l') {
            return (
                <svg
                    x={x}
                    y={y}
                    width={bar_width}
                    height={bar_height}
                    viewBox={viewbox}
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M48 193L48 0.942831C40.0961 0.320274 32.0518 1.78324e-06 23.8943 1.32939e-06C15.8095 8.79582e-07 7.83594 0.314586 1.52073e-05 0.926247L7.69226e-06 193L48 193Z"
                        className="blksail-dynamic"
                    />
                </svg>
            );
        } else {
            return <div>Received an unsupported size!</div>;
        }
    }
}

BarActiveSvgComponent.defaultProps = {};
BarActiveSvgComponent.propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    bar_width: PropTypes.number,
    bar_height: PropTypes.number,
    sml: PropTypes.string,
    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
