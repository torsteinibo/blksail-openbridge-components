import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class ArrowSvgComponent extends Component {
    render() {
        const { x, y, fillClassName, sml } = this.props;
        if (sml === 's') {
            return (
                <svg x={x} y={y} width="6" height="6" viewBox="0 0 6 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M3 0L5.59808 6H0.401924L3 0Z" className={fillClassName} />
                </svg>
            );
        } else if (sml === 'm') {
            return (
                <svg x={x} y={y} width="14" height="16" viewBox="0 0 14 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M7 0L13.9282 18H0.0717969L7 0Z" className={fillClassName} />
                </svg>
            );
        } else if (sml === 'l') {
            return (
                <svg x={x} y={y} width="28" height="36" viewBox="0 0 28 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M14.0001 0L27.8565 36H0.143647L14.0001 0Z" className={fillClassName} />
                </svg>
            );
        } else {
            return <div>Received an invalid size!</div>;
        }
    }
}

ArrowSvgComponent.defaultProps = {};

ArrowSvgComponent.propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    fillClassName: PropTypes.string,
    sml: PropTypes.string,
    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
