import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class HdgArrowSvgComponent extends Component {
  render() {
    if (this.props.sml === 's') {
      return (
        <svg
          x={this.props.x}
          y={this.props.y}
          width="19"
          height="80"
          viewBox="0 0 19 80"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M9.99993 16L17.963 35.3143C18.0687 35.5707 17.9402 35.8617 17.6759 35.9642C17.615 35.9879 17.5501 36 17.4845 36H2.51536C2.23073 36 2 35.7761 2 35.5C2 35.4364 2.01251 35.3734 2.03686 35.3143L9.99993 16Z"
            className="blksail-dynamic"
          />
          <rect x="9" y="36" width="2" height="28" className="blksail-dynamic" />
        </svg>
      );
    } else if (this.props.sml === 'm') {
      return (
        <svg
          x={this.props.x}
          y={this.props.y}
          width="32"
          height="256"
          viewBox="0 0 32 256"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M15.9999 40L31.926 78.6286C32.1374 79.1414 31.8803 79.7234 31.3518 79.9285C31.23 79.9757 31.1001 80 30.969 80H1.03072C0.46147 80 0 79.5523 0 79C0 78.8728 0.0250233 78.7467 0.073723 78.6286L15.9999 40Z"
            className="blksail-dynamic"
          />
          <rect x="15" y="80" width="2" height="136" className="blksail-dynamic" />
        </svg>
      );
    } else if (this.props.sml === 'l') {
      return (
        <svg
          x={this.props.x}
          y={this.props.y}
          width="64"
          height="512"
          viewBox="0 0 64 512"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M31.9997 72L63.852 149.257C64.2748 150.283 63.7607 151.447 62.7036 151.857C62.4601 151.951 62.2003 152 61.938 152H2.06144C0.92294 152 0 151.105 0 150C0 149.746 0.0500466 149.493 0.147446 149.257L31.9997 72Z"
            className="blksail-dynamic"
          />
          <rect x="31" y="64" width="2" height="384" className="blksail-dynamic" />
        </svg>
      );
    }
    return <div>Unsupported size!</div>;
  }
}


HdgArrowSvgComponent.defaultProps = {};

HdgArrowSvgComponent.propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    sml: PropTypes.string,
    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
