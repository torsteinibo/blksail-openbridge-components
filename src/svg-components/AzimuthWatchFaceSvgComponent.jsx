import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class AzimuthWatchFaceSvgComponent extends Component {
    render() {
        const { x, y, sml } = this.props;
        if (sml === 's') {
            return (
                <svg x={x} y={y} width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="32" cy="32" r="28" className="blksail-container-section-stroke" strokeWidth="8" />
                    <g clipPath="url(#azimuthClip0)">
                        <g clipPath="url(#azimuthClip1)">
                            <path d="M54.6274 9.37259L48.9706 15.0294" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M15.0294 48.9706L9.37258 54.6274" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M9.37259 9.37259L15.0294 15.0294" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M48.9706 48.9706L54.6274 54.6274" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        </g>
                        <path d="M32 0L32 8" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        <path d="M32 56L32 64" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        <path d="M0 32L8 32" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        <path d="M56 32L64 32" className="blksail-border-divider-color" strokeOpacity="0.3" />
                    </g>
                    <defs>
                        <clipPath id="azimuthClip0">
                            <rect width="64" height="64" fill="white" />
                        </clipPath>
                        <clipPath id="azimuthClip1">
                            <rect width="64" height="64" fill="white" transform="translate(32 -13.2548) rotate(45)" />
                        </clipPath>
                    </defs>
                </svg>
            );
        } else if (sml === 'm') {
            return (
                <svg
                    x={x}
                    y={y}
                    width="208"
                    height="208"
                    viewBox="0 0 208 208"
                    className="blksail-container-background-fill"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <circle cx="104" cy="104" r="103.5" className="blksail-border-divider-color" strokeOpacity="0.3" />
                    <circle cx="104" cy="104" r="92" className="blksail-container-section-stroke" strokeWidth="8" />
                    <g clipPath="url(#clip0)">
                        <g clipPath="url(#clip1)">
                            <path d="M177.539 30.4609L166.225 41.7746" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M41.7747 166.225L30.4609 177.539" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M30.4609 30.4609L41.7746 41.7746" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M166.225 166.225L177.539 177.539" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        </g>
                        <path d="M104 0L104 16" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        <path d="M104 192L104 208" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        <path d="M0 104L16 104" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        <path d="M192 104L208 104" className="blksail-border-divider-color" strokeOpacity="0.3" />
                    </g>
                    <g clipPath="url(#clip2)">
                        <path d="M104 0L104 8" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        <path d="M104 200L104 208" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        <path d="M0 104L8 104" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        <path d="M200 104L208 104" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        <g clipPath="url(#clip3)">
                            <path
                                d="M94.9358 0.395767L95.633 8.36532"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M112.367 199.635L113.064 207.604"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M0.395752 113.064L8.36531 112.367"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M199.635 95.6331L207.604 94.9358"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip4)">
                            <path
                                d="M85.9407 1.57999L87.3299 9.45845"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M120.67 198.542L122.059 206.42" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M1.58008 122.059L9.45854 120.67" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M198.542 87.3298L206.42 85.9406" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip5)">
                            <path
                                d="M77.0828 3.54373L79.1533 11.2711"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M128.847 196.729L130.917 204.456"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M3.5437 130.917L11.2711 128.847" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M196.729 79.1534L204.456 77.0828"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip6)">
                            <path
                                d="M68.4297 6.27197L71.1658 13.7895"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M136.834 194.211L139.57 201.728" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M6.27173 139.57L13.7893 136.834" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M194.21 71.1661L201.728 68.4299" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip7)">
                            <path
                                d="M60.0475 9.74399L63.4284 16.9945"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M144.571 191.006L147.952 198.256"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M9.74377 147.952L16.9942 144.571"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M191.005 63.4286L198.256 60.0477"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip8)">
                            <path
                                d="M51.9999 13.9333L55.9999 20.8616"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M152 187.138L156 194.067" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M13.9332 156L20.8614 152" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M187.138 56L194.067 52" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip9)">
                            <path
                                d="M44.3478 18.8082L48.9364 25.3614"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M159.063 182.639L163.652 189.192"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M18.808 163.652L25.3612 159.063" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M182.638 48.9367L189.192 44.3481"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip10)">
                            <path
                                d="M37.1498 24.3314L42.2921 30.4597"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M165.707 177.54L170.85 183.669" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M24.3311 170.85L30.4594 165.708" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M177.54 42.2924L183.668 37.1501" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip11)">
                            <path
                                d="M30.4606 30.4609L36.1174 36.1177"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M171.882 171.882L177.539 177.539"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M30.4606 177.539L36.1174 171.882"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M171.882 36.1177L177.539 30.4609"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip12)">
                            <path
                                d="M24.3312 37.1501L30.4595 42.2924"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M177.54 165.708L183.669 170.85" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M37.1499 183.669L42.2922 177.54" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M165.708 30.4597L170.85 24.3313" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip13)">
                            <path
                                d="M18.8079 44.3481L25.3611 48.9367"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M182.638 159.063L189.192 163.652"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M44.3477 189.192L48.9363 182.639"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M159.063 25.3614L163.652 18.8082"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip14)">
                            <path d="M13.9331 52L20.8613 56" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M187.138 152L194.066 156" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M51.9998 194.067L55.9998 187.138"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M152 20.8615L156 13.9333" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip15)">
                            <path
                                d="M9.74365 60.0477L16.9941 63.4286"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M191.005 144.571L198.256 147.952"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M60.0474 198.256L63.4283 191.006"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M144.571 16.9944L147.952 9.74395"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip16)">
                            <path d="M6.27173 68.4299L13.7893 71.166" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M194.21 136.834L201.728 139.57" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M68.4297 201.728L71.1658 194.21" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M136.834 13.7895L139.57 6.27192" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip17)">
                            <path d="M3.5437 77.0828L11.2711 79.1533" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M196.729 128.847L204.456 130.917"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M77.0828 204.456L79.1533 196.729"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M128.847 11.2711L130.917 3.54368"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip18)">
                            <path d="M1.57983 85.9406L9.4583 87.3298" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M198.541 120.67L206.42 122.059" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M85.9404 206.42L87.3296 198.542" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M120.67 9.45847L122.059 1.58" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip19)">
                            <path
                                d="M0.395752 94.9358L8.36531 95.633"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M199.635 112.367L207.604 113.064"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M94.9358 207.604L95.633 199.635" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M112.367 8.3653L113.064 0.395738"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                    </g>
                    <defs>
                        <clipPath id="clip0">
                            <rect width="208" height="208" fill="white" />
                        </clipPath>
                        <clipPath id="clip1">
                            <rect width="208" height="208" fill="white" transform="translate(104 -43.0782) rotate(45)" />
                        </clipPath>
                        <clipPath id="clip2">
                            <rect width="208" height="208" fill="white" />
                        </clipPath>
                        <clipPath id="clip3">
                            <rect width="208" height="208" fill="white" transform="translate(-8.66846 9.45996) rotate(-5)" />
                        </clipPath>
                        <clipPath id="clip4">
                            <rect width="208" height="208" fill="white" transform="translate(-16.4794 19.6394) rotate(-10)" />
                        </clipPath>
                        <clipPath id="clip5">
                            <rect width="208" height="208" fill="white" transform="translate(-23.3735 30.4609) rotate(-15)" />
                        </clipPath>
                        <clipPath id="clip6">
                            <rect width="208" height="208" fill="white" transform="translate(-29.2983 41.8421) rotate(-20)" />
                        </clipPath>
                        <clipPath id="clip7">
                            <rect width="208" height="208" fill="white" transform="translate(-34.2085 53.6963) rotate(-25)" />
                        </clipPath>
                        <clipPath id="clip8">
                            <rect width="208" height="208" fill="white" transform="translate(-38.0668 65.9333) rotate(-30)" />
                        </clipPath>
                        <clipPath id="clip9">
                            <rect width="208" height="208" fill="white" transform="translate(-40.844 78.4601) rotate(-35)" />
                        </clipPath>
                        <clipPath id="clip10">
                            <rect width="208" height="208" fill="white" transform="translate(-42.5188 91.1813) rotate(-40)" />
                        </clipPath>
                        <clipPath id="clip11">
                            <rect width="208" height="208" fill="white" transform="translate(-43.0785 104) rotate(-45)" />
                        </clipPath>
                        <clipPath id="clip12">
                            <rect width="208" height="208" fill="white" transform="translate(-42.5187 116.819) rotate(-50)" />
                        </clipPath>
                        <clipPath id="clip13">
                            <rect width="208" height="208" fill="white" transform="translate(-40.8441 129.54) rotate(-55)" />
                        </clipPath>
                        <clipPath id="clip14">
                            <rect width="208" height="208" fill="white" transform="translate(-38.0669 142.067) rotate(-60)" />
                        </clipPath>
                        <clipPath id="clip15">
                            <rect width="208" height="208" fill="white" transform="translate(-34.2086 154.304) rotate(-65)" />
                        </clipPath>
                        <clipPath id="clip16">
                            <rect width="208" height="208" fill="white" transform="translate(-29.2983 166.158) rotate(-70)" />
                        </clipPath>
                        <clipPath id="clip17">
                            <rect width="208" height="208" fill="white" transform="translate(-23.3735 177.539) rotate(-75)" />
                        </clipPath>
                        <clipPath id="clip18">
                            <rect width="208" height="208" fill="white" transform="translate(-16.4796 188.361) rotate(-80)" />
                        </clipPath>
                        <clipPath id="clip19">
                            <rect width="208" height="208" fill="white" transform="translate(-8.66846 198.54) rotate(-85)" />
                        </clipPath>
                    </defs>
                </svg>
            );
        } else if (sml === 'l') {
            return (
                <svg x={x} y={y} width="416" height="416" viewBox="0 0 416 416" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="208" cy="208" r="188" className="blksail-container-section-stroke" strokeWidth="8" />
                    <g clipPath="url(#clip0)">
                        <path d="M208 0L208 16" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        <path d="M208 400L208 416" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        <path d="M0 208L16 208" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        <path d="M400 208L416 208" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        <g clipPath="url(#clip1)">
                            <path
                                d="M204.37 0.0316772L204.649 16.0292"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M211.351 399.971L211.63 415.968" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M0.0316772 211.63L16.0292 211.351"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M399.971 204.649L415.968 204.37" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip2)">
                            <path d="M200.741 0.12674L201.299 16.117" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M214.701 399.883L215.259 415.873"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M0.126709 215.259L16.117 214.701"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M399.883 201.299L415.873 200.741"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip3)">
                            <path
                                d="M197.114 0.285065L197.952 16.2631"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M218.048 399.737L218.886 415.715"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M0.285034 218.886L16.2631 218.048"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M399.737 197.951L415.715 197.114"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip4)">
                            <path
                                d="M193.491 0.506683L194.607 16.4677"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M221.393 399.532L222.509 415.493"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M0.506653 222.509L16.4677 221.393"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M399.532 194.607L415.493 193.491"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip5)">
                            <path
                                d="M189.872 0.791504L191.266 16.7306"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M224.734 399.269L226.128 415.209"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M0.791504 226.128L16.7306 224.734"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M399.269 191.266L415.209 189.872"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip6)">
                            <path
                                d="M186.258 1.13947L187.931 17.0518"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M228.069 398.948L229.742 414.861"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M1.13943 229.742L17.0518 228.069"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M398.948 187.931L414.861 186.258"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip7)">
                            <path
                                d="M182.651 1.55045L184.601 17.4312"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M231.399 398.569L233.349 414.45" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M1.55042 233.349L17.4312 231.399"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M398.569 184.601L414.45 182.651" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip8)">
                            <path
                                d="M179.052 2.02426L181.279 17.8686"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M234.721 398.131L236.948 413.976"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M2.02423 236.948L17.8685 234.721"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M398.131 181.279L413.976 179.052"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip9)">
                            <path
                                d="M175.462 2.56085L177.965 18.3639"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M238.035 397.636L240.538 413.439"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M2.56082 240.538L18.3638 238.035"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M397.636 177.965L413.439 175.462"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip10)">
                            <path d="M171.881 3.15997L174.66 18.9169" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M241.341 397.083L244.119 412.84" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M3.16013 244.119L18.917 241.34" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M397.083 174.66L412.84 171.881" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip11)">
                            <path
                                d="M168.312 3.82156L171.365 19.5276"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M244.635 396.472L247.688 412.178"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M3.82172 247.688L19.5278 244.635"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M396.473 171.365L412.179 168.312"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip12)">
                            <path
                                d="M164.755 4.54532L168.081 20.1957"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M247.919 395.804L251.246 411.455"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M4.54547 251.246L20.1958 247.919"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M395.805 168.081L411.455 164.754"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip13)">
                            <path d="M161.21 5.33102L164.81 20.9209" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M251.191 395.079L254.79 410.669" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M5.33121 254.79L20.9211 251.191" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M395.079 164.809L410.669 161.21" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip14)">
                            <path d="M157.68 6.1785L161.551 21.7032" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M254.449 394.297L258.32 409.822" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M6.17868 258.32L21.7034 254.449" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M394.297 161.551L409.822 157.68" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip15)">
                            <path
                                d="M154.166 7.08746L158.307 22.5423"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M257.693 393.458L261.834 408.913"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M7.08731 261.834L22.5421 257.693"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M393.458 158.307L408.912 154.166"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip16)">
                            <path
                                d="M150.667 8.05756L155.078 23.4377"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M260.922 392.562L265.332 407.942"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M8.05746 265.333L23.4377 260.922"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M392.562 155.078L407.942 150.667"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip17)">
                            <path
                                d="M147.187 9.08859L151.865 24.3895"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M264.135 391.61L268.813 406.911" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M9.0885 268.813L24.3894 264.135" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M391.61 151.865L406.911 147.187" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip18)">
                            <path
                                d="M143.724 10.1802L148.669 25.3971"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M267.331 390.603L272.275 405.82" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M10.1801 272.276L25.397 267.331" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M390.603 148.669L405.82 143.724" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip19)">
                            <path
                                d="M140.282 11.3322L145.491 26.4604"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M270.509 389.54L275.718 404.668" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M11.332 275.718L26.4603 270.509" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M389.539 145.491L404.668 140.282"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip20)">
                            <path d="M136.86 12.5439L142.332 27.579" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M273.668 388.421L279.14 403.456" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M12.5438 279.14L27.5789 273.668" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M388.421 142.332L403.456 136.86" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip21)">
                            <path
                                d="M133.459 13.8153L139.193 28.7526"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M276.806 387.247L282.54 402.185" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M13.8149 282.541L28.7522 276.807"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M387.247 139.193L402.184 133.46" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip22)">
                            <path
                                d="M130.081 15.1458L136.075 29.9808"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M279.924 386.019L285.918 400.854"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M15.1454 285.918L29.9803 279.925"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M386.019 136.076L400.854 130.082"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip23)">
                            <path
                                d="M126.728 16.5351L132.979 31.2632"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M283.02 384.737L289.272 399.465" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M16.5346 289.272L31.2627 283.02" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M384.737 132.98L399.465 126.728" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip24)">
                            <path
                                d="M123.398 17.9826L129.906 32.5994"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M286.093 383.401L292.601 398.018"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M17.9821 292.601L32.5989 286.094"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M383.4 129.907L398.017 123.399" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip25)">
                            <path d="M120.095 19.488L126.857 33.9889" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M289.142 382.011L295.904 396.512"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M19.4876 295.905L33.9885 289.143"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M382.011 126.857L396.512 120.095"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip26)">
                            <path
                                d="M116.818 21.0508L123.832 35.4315"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M292.167 380.568L299.181 394.949"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M21.0505 299.181L35.4312 292.167"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M380.568 123.833L394.949 116.819"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip27)">
                            <path d="M113.57 22.6706L120.833 36.9267" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M295.166 379.073L302.43 393.329" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M22.6703 302.43L36.9264 295.166" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M379.073 120.834L393.329 113.57" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip28)">
                            <path d="M110.35 24.3469L117.861 38.4741" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M298.138 377.526L305.65 391.653" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M24.3465 305.65L38.4737 298.139" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M377.526 117.862L391.653 110.35" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip29)">
                            <path
                                d="M107.159 26.0792L114.916 40.0731"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M301.083 375.927L308.84 389.921" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M26.0787 308.84L40.0726 301.084" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M375.927 114.917L389.921 107.16" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip30)">
                            <path d="M104 27.8667L112 41.7231" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M304 374.277L312 388.133" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M27.8664 312L41.7228 304" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M374.277 112L388.133 104" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip31)">
                            <path
                                d="M100.872 29.7092L109.112 43.4238"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M306.887 372.576L315.128 386.291"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M29.7089 315.128L43.4235 306.887"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M372.576 109.113L386.29 100.872" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip32)">
                            <path d="M97.7765 31.606L106.255 45.1747" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M309.744 370.825L318.223 384.394"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M31.6057 318.223L45.1744 309.744"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M370.825 106.255L384.394 97.7768"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip33)">
                            <path
                                d="M94.7148 33.5565L103.429 46.9752"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M312.57 369.025L321.285 382.443" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M33.5562 321.285L46.9749 312.571"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M369.024 103.429L382.443 94.7151"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip34)">
                            <path
                                d="M91.6875 35.5602L100.635 48.8248"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M315.365 367.175L324.312 380.44" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M35.5598 324.312L48.8244 315.365"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M367.175 100.635L380.439 91.6879"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip35)">
                            <path
                                d="M88.6956 37.6164L97.8728 50.7228"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M318.126 365.277L327.303 378.384"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M37.6159 327.304L50.7223 318.127"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M365.277 97.8733L378.383 88.6961"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip36)">
                            <path
                                d="M85.7401 39.7244L95.1447 52.6687"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M320.854 363.331L330.259 376.276"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M39.7239 330.259L52.6682 320.855"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M363.331 95.1452L376.275 85.7406"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip37)">
                            <path d="M82.822 41.8838L92.451 54.662" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M323.548 361.338L333.177 374.116"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M41.8833 333.177L54.6615 323.548"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M361.338 92.4515L374.116 82.8225"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip38)">
                            <path d="M79.9419 44.0938L89.7925 56.702" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M326.207 359.298L336.057 371.906"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M44.0933 336.058L56.7014 326.207"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M359.298 89.793L371.906 79.9424" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip39)">
                            <path d="M77.1009 46.3536L87.17 58.788" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M328.829 357.212L338.898 369.646"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M46.3531 338.899L58.7875 328.829"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M357.212 87.1705L369.646 77.1013"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip40)">
                            <path
                                d="M74.2997 48.6627L84.5843 60.9194"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M331.415 355.081L341.699 367.337"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M48.6623 341.7L60.919 331.415" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M355.08 84.5847L367.337 74.3001" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip41)">
                            <path
                                d="M71.5392 51.0204L82.0362 63.0957"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M333.963 352.904L344.46 364.98" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M51.0199 344.46L63.0953 333.963" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M352.904 82.0367L364.979 71.5397"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip42)">
                            <path
                                d="M68.8203 53.4258L79.5264 65.3162"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M336.473 350.684L347.179 362.574"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M53.4254 347.179L65.3157 336.473"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M350.683 79.5269L362.574 68.8208"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip43)">
                            <path
                                d="M66.1439 55.8784L77.0559 67.5801"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M338.943 348.42L349.855 360.122" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M55.878 349.856L67.5796 338.944" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M348.419 77.0563L360.121 66.1443"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip44)">
                            <path
                                d="M63.5106 58.3773L74.6251 69.8868"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M341.374 346.113L352.488 357.623"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M58.3769 352.489L69.8863 341.374"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M346.113 74.6256L357.622 63.511" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip45)">
                            <path
                                d="M60.9212 60.9217L72.2349 72.2354"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M343.764 343.764L355.078 355.078"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M60.9212 355.078L72.2349 343.764"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M343.764 72.2354L355.078 60.9217"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip46)">
                            <path d="M58.3767 63.511L69.8862 74.6255" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M346.113 341.374L357.622 352.489"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M63.5105 357.623L74.625 346.113" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M341.374 69.8867L352.488 58.3773"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip47)">
                            <path
                                d="M55.8778 66.1443L67.5795 77.0563"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M348.419 338.944L360.121 349.856"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M66.1438 360.122L77.0557 348.42" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M338.943 67.58L349.855 55.8784" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip48)">
                            <path
                                d="M53.4253 68.8208L65.3156 79.5269"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M350.683 336.473L362.574 347.179"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M68.8203 362.574L79.5264 350.684"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M336.473 65.3161L347.179 53.4258"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip49)">
                            <path
                                d="M51.0198 71.5397L63.0952 82.0366"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M352.904 333.963L364.979 344.46" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M71.5392 364.98L82.0361 352.904" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M333.963 63.0957L344.46 51.0204" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip50)">
                            <path
                                d="M48.6624 74.3001L60.9192 84.5847"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M355.08 331.415L367.337 341.7" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M74.2999 367.337L84.5845 355.08" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M331.415 60.9194L341.7 48.6627" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip51)">
                            <path
                                d="M46.3533 77.1013L58.7877 87.1704"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M357.212 328.829L369.646 338.899"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M77.101 369.646L87.1702 357.212" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M328.829 58.788L338.898 46.3536" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip52)">
                            <path d="M44.0934 79.9424L56.7016 89.793" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M359.298 326.207L371.906 336.058"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M79.9421 371.906L89.7927 359.298"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M326.207 56.702L336.057 44.0938" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip53)">
                            <path
                                d="M41.8835 82.8225L54.6617 92.4516"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M361.338 323.549L374.116 333.178"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M82.8221 374.116L92.4512 361.338"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M323.548 54.662L333.177 41.8839" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip54)">
                            <path
                                d="M39.7242 85.7407L52.6684 95.1453"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M363.331 320.855L376.275 330.259"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M85.7404 376.276L95.1449 363.331"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M320.854 52.6688L330.259 39.7245"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip55)">
                            <path
                                d="M37.6158 88.6961L50.7222 97.8733"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M365.277 318.127L378.383 327.304"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M88.6955 378.384L97.8727 365.277"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M318.126 50.7228L327.303 37.6164"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip56)">
                            <path
                                d="M35.5596 91.6878L48.8242 100.635"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M367.175 315.365L380.439 324.312"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M91.6873 380.44L100.634 367.175" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M315.365 48.8247L324.312 35.5601"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip57)">
                            <path d="M33.556 94.715L46.9747 103.429" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M369.024 312.571L382.443 321.285"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M94.7145 382.443L103.429 369.025"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M312.57 46.9752L321.284 33.5565" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip58)">
                            <path
                                d="M31.6054 97.7768L45.1742 106.255"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M370.825 309.745L384.393 318.223"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M97.7762 384.394L106.255 370.825"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M309.744 45.1747L318.223 31.606" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip59)">
                            <path
                                d="M29.7086 100.872L43.4233 109.113"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M372.576 306.887L386.29 315.128" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M100.871 386.291L109.112 372.576"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M306.887 43.424L315.127 29.7093" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip60)">
                            <path d="M27.8662 104L41.7226 112" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M374.276 304L388.133 312" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M104 388.133L112 374.277" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M304 41.7231L312 27.8667" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip61)">
                            <path
                                d="M26.0786 107.159L40.0725 114.916"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M375.927 301.083L389.92 308.84" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M107.159 389.921L114.916 375.927"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M301.083 40.0728L308.84 26.0789" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip62)">
                            <path d="M24.3464 110.35L38.4736 117.861" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M377.526 298.138L391.653 305.65" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M110.349 391.653L117.861 377.526"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M298.138 38.4739L305.65 24.3467" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip63)">
                            <path d="M22.6702 113.57L36.9263 120.834" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M379.073 295.166L393.329 302.43" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M113.57 393.329L120.833 379.073" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M295.166 36.9266L302.43 22.6705" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip64)">
                            <path
                                d="M21.0504 116.819L35.4311 123.832"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M380.568 292.167L394.949 299.181"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M116.818 394.949L123.832 380.568"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M292.167 35.4313L299.181 21.0506"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip65)">
                            <path
                                d="M19.4875 120.095L33.9884 126.857"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M382.011 289.143L396.512 295.904"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M120.095 396.512L126.857 382.011"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M289.142 33.9888L295.904 19.4878"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip66)">
                            <path d="M17.982 123.399L32.5988 129.906" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M383.4 286.093L398.017 292.601" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M123.398 398.017L129.906 383.401"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M286.093 32.5992L292.601 17.9825"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip67)">
                            <path d="M16.5345 126.728L31.2626 132.98" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M384.736 283.02L399.464 289.272" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M126.727 399.465L132.979 384.737"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M283.02 31.263L289.272 16.5349" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip68)">
                            <path
                                d="M15.1453 130.082L29.9802 136.075"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M386.019 279.924L400.854 285.918"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M130.081 400.854L136.075 386.019"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M279.924 29.9807L285.918 15.1457"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip69)">
                            <path d="M13.8148 133.459L28.752 139.193" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M387.247 276.807L402.184 282.54" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M133.459 402.185L139.193 387.247"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M276.806 28.7525L282.54 13.8152" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip70)">
                            <path d="M12.5434 136.86L27.5785 142.332" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M388.42 273.668L403.456 279.14" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M136.859 403.456L142.332 388.421"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M273.667 27.5789L279.14 12.5439" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip71)">
                            <path
                                d="M11.3318 140.282L26.4601 145.491"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M389.539 270.509L404.668 275.718"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M140.281 404.668L145.491 389.54" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M270.509 26.4604L275.718 11.3321"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip72)">
                            <path
                                d="M10.1799 143.724L25.3968 148.669"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M390.603 267.331L405.819 272.276"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M143.724 405.82L148.668 390.603" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M267.331 25.3971L272.275 10.1802"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip73)">
                            <path
                                d="M9.08829 147.187L24.3892 151.865"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M391.61 264.135L406.911 268.813" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M147.186 406.911L151.864 391.61" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M264.135 24.3894L268.813 9.08856"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip74)">
                            <path
                                d="M8.05722 150.667L23.4374 155.078"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M392.562 260.922L407.942 265.333"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M150.667 407.942L155.077 392.562"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M260.922 23.4378L265.332 8.05759"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip75)">
                            <path d="M7.08719 154.166L22.542 158.307" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M393.458 257.693L408.912 261.834"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M154.165 408.913L158.307 393.458"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M257.693 22.5422L261.834 7.08742"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip76)">
                            <path d="M6.17825 157.68L21.703 161.551" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M394.297 254.449L409.821 258.32" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M157.68 409.822L161.551 394.297" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M254.449 21.7033L258.32 6.17858" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip77)">
                            <path d="M5.33081 161.21L20.9207 164.809" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M395.079 251.191L410.669 254.79" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M161.21 410.669L164.809 395.079" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M251.19 20.921L254.79 5.33104" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip78)">
                            <path
                                d="M4.54507 164.754L20.1954 168.081"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M395.804 247.919L411.454 251.246"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M164.754 411.455L168.081 395.804"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M247.919 20.1956L251.245 4.54523"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip79)">
                            <path
                                d="M3.82132 168.312L19.5274 171.365"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M396.472 244.635L412.178 247.688"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M168.311 412.178L171.364 396.472"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M244.635 19.5274L247.688 3.8214" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip80)">
                            <path d="M3.15961 171.881L18.9165 174.66" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M397.083 241.34L412.84 244.119" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M171.881 412.84L174.659 397.083" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M241.34 18.9169L244.118 3.16001" className="blksail-border-divider-color" strokeOpacity="0.12" />
                        </g>
                        <g clipPath="url(#clip81)">
                            <path
                                d="M2.56046 175.462L18.3635 177.965"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M397.636 238.035L413.439 240.538"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M175.461 413.439L177.964 397.636"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M238.035 18.3638L240.538 2.56076"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip82)">
                            <path
                                d="M2.02386 179.052L17.8682 181.279"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M398.131 234.721L413.975 236.948"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M179.052 413.976L181.278 398.131"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M234.721 17.8685L236.948 2.02421"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip83)">
                            <path
                                d="M1.55002 182.651L17.4308 184.601"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M398.568 231.399L414.449 233.349"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M182.651 414.45L184.601 398.569" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M231.399 17.4311L233.348 1.55032"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip84)">
                            <path d="M1.13907 186.258L17.0514 187.93" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M398.948 228.069L414.86 229.742" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M186.258 414.86L187.93 398.948" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M228.069 17.0517L229.742 1.13935"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip85)">
                            <path
                                d="M0.791382 189.872L16.7305 191.266"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M399.269 224.734L415.208 226.128"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M189.872 415.208L191.266 399.269"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M224.734 16.7306L226.128 0.791476"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip86)">
                            <path
                                d="M0.506561 193.491L16.4676 194.607"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M399.532 221.393L415.493 222.509"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M193.491 415.493L194.607 399.532"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M221.393 16.4678L222.509 0.506748"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip87)">
                            <path
                                d="M0.284943 197.114L16.263 197.952"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M399.737 218.049L415.715 218.886"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M197.114 415.715L197.951 399.737"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M218.048 16.2632L218.886 0.285112"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip88)">
                            <path
                                d="M0.126617 200.741L16.1169 201.299"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M399.883 214.701L415.873 215.259"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M200.741 415.873L201.299 399.883"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path
                                d="M214.701 16.117L215.259 0.126751"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                        <g clipPath="url(#clip89)">
                            <path
                                d="M0.0315857 204.37L16.0291 204.649"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                            <path d="M399.971 211.351L415.968 211.63" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path d="M204.37 415.968L204.649 399.971" className="blksail-border-divider-color" strokeOpacity="0.12" />
                            <path
                                d="M211.351 16.0292L211.63 0.0316423"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.12"
                            />
                        </g>
                    </g>
                    <g clipPath="url(#clip90)">
                        <path d="M208 0L208 24" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        <path d="M208 392L208 416" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        <path d="M0 208L24 208" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        <path d="M392 208L416 208" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        <g clipPath="url(#clip91)">
                            <path
                                d="M189.872 0.791504L191.963 24.7002"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.3"
                            />
                            <path d="M224.037 391.3L226.128 415.208" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path
                                d="M0.791504 226.128L24.7002 224.037"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.3"
                            />
                            <path d="M391.3 191.963L415.209 189.872" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        </g>
                        <g clipPath="url(#clip92)">
                            <path d="M171.881 3.15997L176.049 26.7954" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M239.951 389.205L244.119 412.84" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M3.16013 244.119L26.7955 239.951" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M389.205 176.049L412.84 171.881" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        </g>
                        <g clipPath="url(#clip93)">
                            <path d="M154.166 7.08746L160.377 30.2697" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M255.623 385.73L261.834 408.913" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M7.08731 261.834L30.2695 255.623" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M385.73 160.377L408.912 154.166" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        </g>
                        <g clipPath="url(#clip94)">
                            <path d="M136.859 12.5439L145.068 35.0966" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M270.931 380.903L279.14 403.456" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M12.5435 279.14L35.0962 270.932" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M380.903 145.068L403.456 136.86" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        </g>
                        <g clipPath="url(#clip95)">
                            <path d="M120.095 19.488L130.238 41.2394" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M285.761 374.761L295.904 396.512" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M19.4876 295.905L41.239 285.762" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M374.76 130.238L396.512 120.095" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        </g>
                        <g clipPath="url(#clip96)">
                            <path d="M104 27.8667L116 48.6513" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M300 367.349L312 388.133" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M27.8664 312L48.651 300" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M367.348 116L388.133 104" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        </g>
                        <g clipPath="url(#clip97)">
                            <path d="M88.6956 37.6164L102.461 57.276" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M313.538 358.724L327.303 378.384" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M37.6159 327.304L57.2755 313.538" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M358.723 102.462L378.383 88.6961" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        </g>
                        <g clipPath="url(#clip98)">
                            <path d="M74.2997 48.6627L89.7266 67.0478" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M326.272 348.952L341.699 367.337" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M48.6623 341.7L67.0474 326.273" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M348.952 89.7271L367.337 74.3001" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        </g>
                        <g clipPath="url(#clip99)">
                            <path d="M60.9212 60.9218L77.8918 77.8923" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M338.107 338.108L355.078 355.078" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M60.9212 355.078L77.8918 338.108" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M338.107 77.8923L355.078 60.9218" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        </g>
                        <g clipPath="url(#clip100)">
                            <path d="M48.6624 74.3001L67.0475 89.727" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M348.952 326.273L367.337 341.7" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M74.2999 367.337L89.7268 348.952" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M326.273 67.0477L341.699 48.6627" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        </g>
                        <g clipPath="url(#clip101)">
                            <path d="M37.6158 88.6961L57.2755 102.462" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M358.723 313.538L378.383 327.304" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M88.6955 378.384L102.461 358.724" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M313.537 57.276L327.303 37.6164" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        </g>
                        <g clipPath="url(#clip102)">
                            <path d="M27.8663 104L48.6509 116" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M367.348 300L388.133 312" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M104 388.133L116 367.349" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M300 48.6512L312 27.8666" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        </g>
                        <g clipPath="url(#clip103)">
                            <path d="M19.4875 120.095L41.2389 130.238" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M374.76 285.762L396.512 295.904" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M120.095 396.512L130.238 374.761" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M285.761 41.2392L295.904 19.4878" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        </g>
                        <g clipPath="url(#clip104)">
                            <path d="M12.5436 136.86L35.0962 145.068" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M380.903 270.932L403.456 279.14" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M136.859 403.456L145.068 380.903" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M270.931 35.0965L279.14 12.5439" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        </g>
                        <g clipPath="url(#clip105)">
                            <path d="M7.08719 154.166L30.2694 160.377" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M385.73 255.623L408.912 261.834" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M154.165 408.913L160.377 385.73" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M255.622 30.2696L261.834 7.0874" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        </g>
                        <g clipPath="url(#clip106)">
                            <path d="M3.15961 171.881L26.795 176.049" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M389.204 239.951L412.84 244.119" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M171.881 412.84L176.048 389.205" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M239.951 26.7954L244.118 3.15999" className="blksail-border-divider-color" strokeOpacity="0.3" />
                        </g>
                        <g clipPath="url(#clip107)">
                            <path
                                d="M0.791412 189.872L24.7001 191.963"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.3"
                            />
                            <path d="M391.3 224.037L415.208 226.128" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path d="M189.872 415.208L191.963 391.3" className="blksail-border-divider-color" strokeOpacity="0.3" />
                            <path
                                d="M224.037 24.7001L226.128 0.791461"
                                className="blksail-border-divider-color"
                                strokeOpacity="0.3"
                            />
                        </g>
                    </g>
                    <g clipPath="url(#clip108)">
                        <g clipPath="url(#clip109)">
                            <path d="M355.078 60.9218L338.108 77.8923" className="blksail-border-divider-color" strokeWidth="2" />
                            <path d="M77.8924 338.108L60.9218 355.078" className="blksail-border-divider-color" strokeWidth="2" />
                            <path d="M60.9218 60.9218L77.8923 77.8923" className="blksail-border-divider-color" strokeWidth="2" />
                            <path d="M338.108 338.108L355.078 355.078" className="blksail-border-divider-color" strokeWidth="2" />
                        </g>
                        <path d="M208 0L208 24" className="blksail-border-divider-color" strokeWidth="2" />
                        <path d="M208 392L208 416" className="blksail-border-divider-color" strokeWidth="2" />
                        <path d="M0 208L24 208" className="blksail-border-divider-color" strokeWidth="2" />
                        <path d="M392 208L416 208" className="blksail-border-divider-color" strokeWidth="2" />
                    </g>
                    <circle cx="208" cy="208" r="207.5" className="blksail-border-divider-color" strokeOpacity="0.3" />
                    <defs>
                        <clipPath id="clip0">
                            <rect width="416" height="416" fill="white" />
                        </clipPath>
                        <clipPath id="clip1">
                            <rect width="416" height="416" fill="white" transform="translate(-3.59842 3.66177) rotate(-1)" />
                        </clipPath>
                        <clipPath id="clip2">
                            <rect width="416" height="416" fill="white" transform="translate(-7.13239 7.38583) rotate(-2)" />
                        </clipPath>
                        <clipPath id="clip3">
                            <rect width="416" height="416" fill="white" transform="translate(-10.6008 11.1709) rotate(-3)" />
                        </clipPath>
                        <clipPath id="clip4">
                            <rect width="416" height="416" fill="white" transform="translate(-14.0027 15.016) rotate(-4)" />
                        </clipPath>
                        <clipPath id="clip5">
                            <rect width="416" height="416" fill="white" transform="translate(-17.3369 18.9199) rotate(-5)" />
                        </clipPath>
                        <clipPath id="clip6">
                            <rect width="416" height="416" fill="white" transform="translate(-20.6025 22.8814) rotate(-6)" />
                        </clipPath>
                        <clipPath id="clip7">
                            <rect width="416" height="416" fill="white" transform="translate(-23.7984 26.8993) rotate(-7)" />
                        </clipPath>
                        <clipPath id="clip8">
                            <rect width="416" height="416" fill="white" transform="translate(-26.9238 30.9723) rotate(-8)" />
                        </clipPath>
                        <clipPath id="clip9">
                            <rect width="416" height="416" fill="white" transform="translate(-29.9775 35.0992) rotate(-9)" />
                        </clipPath>
                        <clipPath id="clip10">
                            <rect width="416" height="416" fill="white" transform="translate(-32.9587 39.2788) rotate(-10)" />
                        </clipPath>
                        <clipPath id="clip11">
                            <rect width="416" height="416" fill="white" transform="translate(-35.8665 43.5098) rotate(-11)" />
                        </clipPath>
                        <clipPath id="clip12">
                            <rect width="416" height="416" fill="white" transform="translate(-38.7002 47.791) rotate(-12)" />
                        </clipPath>
                        <clipPath id="clip13">
                            <rect width="416" height="416" fill="white" transform="translate(-41.4586 52.1208) rotate(-13)" />
                        </clipPath>
                        <clipPath id="clip14">
                            <rect width="416" height="416" fill="white" transform="translate(-44.1411 56.4983) rotate(-14)" />
                        </clipPath>
                        <clipPath id="clip15">
                            <rect width="416" height="416" fill="white" transform="translate(-46.747 60.9218) rotate(-15)" />
                        </clipPath>
                        <clipPath id="clip16">
                            <rect width="416" height="416" fill="white" transform="translate(-49.2751 65.3901) rotate(-16)" />
                        </clipPath>
                        <clipPath id="clip17">
                            <rect width="416" height="416" fill="white" transform="translate(-51.7248 69.9019) rotate(-17)" />
                        </clipPath>
                        <clipPath id="clip18">
                            <rect width="416" height="416" fill="white" transform="translate(-54.0954 74.4557) rotate(-18)" />
                        </clipPath>
                        <clipPath id="clip19">
                            <rect width="416" height="416" fill="white" transform="translate(-56.3861 79.0503) rotate(-19)" />
                        </clipPath>
                        <clipPath id="clip20">
                            <rect width="416" height="416" fill="white" transform="translate(-58.5964 83.6841) rotate(-20)" />
                        </clipPath>
                        <clipPath id="clip21">
                            <rect width="416" height="416" fill="white" transform="translate(-60.7256 88.3559) rotate(-21)" />
                        </clipPath>
                        <clipPath id="clip22">
                            <rect width="416" height="416" fill="white" transform="translate(-62.7728 93.064) rotate(-22)" />
                        </clipPath>
                        <clipPath id="clip23">
                            <rect width="416" height="416" fill="white" transform="translate(-64.7375 97.8072) rotate(-23)" />
                        </clipPath>
                        <clipPath id="clip24">
                            <rect width="416" height="416" fill="white" transform="translate(-66.6191 102.584) rotate(-24)" />
                        </clipPath>
                        <clipPath id="clip25">
                            <rect width="416" height="416" fill="white" transform="translate(-68.417 107.393) rotate(-25)" />
                        </clipPath>
                        <clipPath id="clip26">
                            <rect width="416" height="416" fill="white" transform="translate(-70.1307 112.232) rotate(-26)" />
                        </clipPath>
                        <clipPath id="clip27">
                            <rect width="416" height="416" fill="white" transform="translate(-71.7598 117.101) rotate(-27)" />
                        </clipPath>
                        <clipPath id="clip28">
                            <rect width="416" height="416" fill="white" transform="translate(-73.3036 121.997) rotate(-28)" />
                        </clipPath>
                        <clipPath id="clip29">
                            <rect width="416" height="416" fill="white" transform="translate(-74.7617 126.92) rotate(-29)" />
                        </clipPath>
                        <clipPath id="clip30">
                            <rect width="416" height="416" fill="white" transform="translate(-76.1336 131.867) rotate(-30)" />
                        </clipPath>
                        <clipPath id="clip31">
                            <rect width="416" height="416" fill="white" transform="translate(-77.4191 136.837) rotate(-31)" />
                        </clipPath>
                        <clipPath id="clip32">
                            <rect width="416" height="416" fill="white" transform="translate(-78.6176 141.829) rotate(-32)" />
                        </clipPath>
                        <clipPath id="clip33">
                            <rect width="416" height="416" fill="white" transform="translate(-79.7287 146.841) rotate(-33)" />
                        </clipPath>
                        <clipPath id="clip34">
                            <rect width="416" height="416" fill="white" transform="translate(-80.7523 151.872) rotate(-34)" />
                        </clipPath>
                        <clipPath id="clip35">
                            <rect width="416" height="416" fill="white" transform="translate(-81.688 156.92) rotate(-35)" />
                        </clipPath>
                        <clipPath id="clip36">
                            <rect width="416" height="416" fill="white" transform="translate(-82.5354 161.984) rotate(-36)" />
                        </clipPath>
                        <clipPath id="clip37">
                            <rect width="416" height="416" fill="white" transform="translate(-83.2942 167.061) rotate(-37)" />
                        </clipPath>
                        <clipPath id="clip38">
                            <rect width="416" height="416" fill="white" transform="translate(-83.9643 172.151) rotate(-38)" />
                        </clipPath>
                        <clipPath id="clip39">
                            <rect width="416" height="416" fill="white" transform="translate(-84.5455 177.252) rotate(-39)" />
                        </clipPath>
                        <clipPath id="clip40">
                            <rect width="416" height="416" fill="white" transform="translate(-85.0375 182.363) rotate(-40)" />
                        </clipPath>
                        <clipPath id="clip41">
                            <rect width="416" height="416" fill="white" transform="translate(-85.4403 187.481) rotate(-41)" />
                        </clipPath>
                        <clipPath id="clip42">
                            <rect width="416" height="416" fill="white" transform="translate(-85.7538 192.605) rotate(-42)" />
                        </clipPath>
                        <clipPath id="clip43">
                            <rect width="416" height="416" fill="white" transform="translate(-85.9777 197.734) rotate(-43)" />
                        </clipPath>
                        <clipPath id="clip44">
                            <rect width="416" height="416" fill="white" transform="translate(-86.1121 202.866) rotate(-44)" />
                        </clipPath>
                        <clipPath id="clip45">
                            <rect width="416" height="416" fill="white" transform="translate(-86.157 208) rotate(-45)" />
                        </clipPath>
                        <clipPath id="clip46">
                            <rect width="416" height="416" fill="white" transform="translate(-86.1122 213.134) rotate(-46)" />
                        </clipPath>
                        <clipPath id="clip47">
                            <rect width="416" height="416" fill="white" transform="translate(-85.9778 218.266) rotate(-47)" />
                        </clipPath>
                        <clipPath id="clip48">
                            <rect width="416" height="416" fill="white" transform="translate(-85.7539 223.395) rotate(-48)" />
                        </clipPath>
                        <clipPath id="clip49">
                            <rect width="416" height="416" fill="white" transform="translate(-85.4404 228.519) rotate(-49)" />
                        </clipPath>
                        <clipPath id="clip50">
                            <rect width="416" height="416" fill="white" transform="translate(-85.0374 233.637) rotate(-50)" />
                        </clipPath>
                        <clipPath id="clip51">
                            <rect width="416" height="416" fill="white" transform="translate(-84.5453 238.748) rotate(-51)" />
                        </clipPath>
                        <clipPath id="clip52">
                            <rect width="416" height="416" fill="white" transform="translate(-83.9641 243.849) rotate(-52)" />
                        </clipPath>
                        <clipPath id="clip53">
                            <rect width="416" height="416" fill="white" transform="translate(-83.294 248.939) rotate(-53)" />
                        </clipPath>
                        <clipPath id="clip54">
                            <rect width="416" height="416" fill="white" transform="translate(-82.5352 254.016) rotate(-54)" />
                        </clipPath>
                        <clipPath id="clip55">
                            <rect width="416" height="416" fill="white" transform="translate(-81.6881 259.08) rotate(-55)" />
                        </clipPath>
                        <clipPath id="clip56">
                            <rect width="416" height="416" fill="white" transform="translate(-80.7525 264.128) rotate(-56)" />
                        </clipPath>
                        <clipPath id="clip57">
                            <rect width="416" height="416" fill="white" transform="translate(-79.729 269.159) rotate(-57)" />
                        </clipPath>
                        <clipPath id="clip58">
                            <rect width="416" height="416" fill="white" transform="translate(-78.6178 274.171) rotate(-58)" />
                        </clipPath>
                        <clipPath id="clip59">
                            <rect width="416" height="416" fill="white" transform="translate(-77.4193 279.163) rotate(-59)" />
                        </clipPath>
                        <clipPath id="clip60">
                            <rect width="416" height="416" fill="white" transform="translate(-76.1337 284.133) rotate(-60)" />
                        </clipPath>
                        <clipPath id="clip61">
                            <rect width="416" height="416" fill="white" transform="translate(-74.7618 289.08) rotate(-61)" />
                        </clipPath>
                        <clipPath id="clip62">
                            <rect width="416" height="416" fill="white" transform="translate(-73.3036 294.003) rotate(-62)" />
                        </clipPath>
                        <clipPath id="clip63">
                            <rect width="416" height="416" fill="white" transform="translate(-71.7598 298.899) rotate(-63)" />
                        </clipPath>
                        <clipPath id="clip64">
                            <rect width="416" height="416" fill="white" transform="translate(-70.1308 303.768) rotate(-64)" />
                        </clipPath>
                        <clipPath id="clip65">
                            <rect width="416" height="416" fill="white" transform="translate(-68.4171 308.607) rotate(-65)" />
                        </clipPath>
                        <clipPath id="clip66">
                            <rect width="416" height="416" fill="white" transform="translate(-66.6192 313.416) rotate(-66)" />
                        </clipPath>
                        <clipPath id="clip67">
                            <rect width="416" height="416" fill="white" transform="translate(-64.7376 318.193) rotate(-67)" />
                        </clipPath>
                        <clipPath id="clip68">
                            <rect width="416" height="416" fill="white" transform="translate(-62.7729 322.936) rotate(-68)" />
                        </clipPath>
                        <clipPath id="clip69">
                            <rect width="416" height="416" fill="white" transform="translate(-60.7258 327.644) rotate(-69)" />
                        </clipPath>
                        <clipPath id="clip70">
                            <rect width="416" height="416" fill="white" transform="translate(-58.5968 332.316) rotate(-70)" />
                        </clipPath>
                        <clipPath id="clip71">
                            <rect width="416" height="416" fill="white" transform="translate(-56.3864 336.95) rotate(-71)" />
                        </clipPath>
                        <clipPath id="clip72">
                            <rect width="416" height="416" fill="white" transform="translate(-54.0956 341.544) rotate(-72)" />
                        </clipPath>
                        <clipPath id="clip73">
                            <rect width="416" height="416" fill="white" transform="translate(-51.725 346.098) rotate(-73)" />
                        </clipPath>
                        <clipPath id="clip74">
                            <rect width="416" height="416" fill="white" transform="translate(-49.2753 350.61) rotate(-74)" />
                        </clipPath>
                        <clipPath id="clip75">
                            <rect width="416" height="416" fill="white" transform="translate(-46.7472 355.078) rotate(-75)" />
                        </clipPath>
                        <clipPath id="clip76">
                            <rect width="416" height="416" fill="white" transform="translate(-44.1415 359.502) rotate(-76)" />
                        </clipPath>
                        <clipPath id="clip77">
                            <rect width="416" height="416" fill="white" transform="translate(-41.459 363.879) rotate(-77)" />
                        </clipPath>
                        <clipPath id="clip78">
                            <rect width="416" height="416" fill="white" transform="translate(-38.7006 368.209) rotate(-78)" />
                        </clipPath>
                        <clipPath id="clip79">
                            <rect width="416" height="416" fill="white" transform="translate(-35.8669 372.49) rotate(-79)" />
                        </clipPath>
                        <clipPath id="clip80">
                            <rect width="416" height="416" fill="white" transform="translate(-32.9592 376.721) rotate(-80)" />
                        </clipPath>
                        <clipPath id="clip81">
                            <rect width="416" height="416" fill="white" transform="translate(-29.9779 380.901) rotate(-81)" />
                        </clipPath>
                        <clipPath id="clip82">
                            <rect width="416" height="416" fill="white" transform="translate(-26.9241 385.028) rotate(-82)" />
                        </clipPath>
                        <clipPath id="clip83">
                            <rect width="416" height="416" fill="white" transform="translate(-23.7988 389.101) rotate(-83)" />
                        </clipPath>
                        <clipPath id="clip84">
                            <rect width="416" height="416" fill="white" transform="translate(-20.6028 393.119) rotate(-84)" />
                        </clipPath>
                        <clipPath id="clip85">
                            <rect width="416" height="416" fill="white" transform="translate(-17.337 397.08) rotate(-85)" />
                        </clipPath>
                        <clipPath id="clip86">
                            <rect width="416" height="416" fill="white" transform="translate(-14.0028 400.984) rotate(-86)" />
                        </clipPath>
                        <clipPath id="clip87">
                            <rect width="416" height="416" fill="white" transform="translate(-10.6009 404.829) rotate(-87)" />
                        </clipPath>
                        <clipPath id="clip88">
                            <rect width="416" height="416" fill="white" transform="translate(-7.13248 408.614) rotate(-88)" />
                        </clipPath>
                        <clipPath id="clip89">
                            <rect width="416" height="416" fill="white" transform="translate(-3.59851 412.338) rotate(-89)" />
                        </clipPath>
                        <clipPath id="clip90">
                            <rect width="416" height="416" fill="white" />
                        </clipPath>
                        <clipPath id="clip91">
                            <rect width="416" height="416" fill="white" transform="translate(-17.3369 18.9199) rotate(-5)" />
                        </clipPath>
                        <clipPath id="clip92">
                            <rect width="416" height="416" fill="white" transform="translate(-32.9587 39.2788) rotate(-10)" />
                        </clipPath>
                        <clipPath id="clip93">
                            <rect width="416" height="416" fill="white" transform="translate(-46.747 60.9218) rotate(-15)" />
                        </clipPath>
                        <clipPath id="clip94">
                            <rect width="416" height="416" fill="white" transform="translate(-58.5966 83.6841) rotate(-20)" />
                        </clipPath>
                        <clipPath id="clip95">
                            <rect width="416" height="416" fill="white" transform="translate(-68.417 107.393) rotate(-25)" />
                        </clipPath>
                        <clipPath id="clip96">
                            <rect width="416" height="416" fill="white" transform="translate(-76.1336 131.867) rotate(-30)" />
                        </clipPath>
                        <clipPath id="clip97">
                            <rect width="416" height="416" fill="white" transform="translate(-81.688 156.92) rotate(-35)" />
                        </clipPath>
                        <clipPath id="clip98">
                            <rect width="416" height="416" fill="white" transform="translate(-85.0375 182.363) rotate(-40)" />
                        </clipPath>
                        <clipPath id="clip99">
                            <rect width="416" height="416" fill="white" transform="translate(-86.157 208) rotate(-45)" />
                        </clipPath>
                        <clipPath id="clip100">
                            <rect width="416" height="416" fill="white" transform="translate(-85.0374 233.637) rotate(-50)" />
                        </clipPath>
                        <clipPath id="clip101">
                            <rect width="416" height="416" fill="white" transform="translate(-81.6881 259.08) rotate(-55)" />
                        </clipPath>
                        <clipPath id="clip102">
                            <rect width="416" height="416" fill="white" transform="translate(-76.1337 284.133) rotate(-60)" />
                        </clipPath>
                        <clipPath id="clip103">
                            <rect width="416" height="416" fill="white" transform="translate(-68.4171 308.607) rotate(-65)" />
                        </clipPath>
                        <clipPath id="clip104">
                            <rect width="416" height="416" fill="white" transform="translate(-58.5966 332.316) rotate(-70)" />
                        </clipPath>
                        <clipPath id="clip105">
                            <rect width="416" height="416" fill="white" transform="translate(-46.7472 355.078) rotate(-75)" />
                        </clipPath>
                        <clipPath id="clip106">
                            <rect width="416" height="416" fill="white" transform="translate(-32.9592 376.721) rotate(-80)" />
                        </clipPath>
                        <clipPath id="clip107">
                            <rect width="416" height="416" fill="white" transform="translate(-17.337 397.08) rotate(-85)" />
                        </clipPath>
                        <clipPath id="clip108">
                            <rect width="416" height="416" fill="white" />
                        </clipPath>
                        <clipPath id="clip109">
                            <rect width="416" height="416" fill="white" transform="translate(208 -86.1564) rotate(45)" />
                        </clipPath>
                    </defs>
                </svg>
            );
        }

        return <div>Received an unsupported size!</div>;
    }
}

AzimuthWatchFaceSvgComponent.defaultProps = {};

AzimuthWatchFaceSvgComponent.propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    sml: PropTypes.string,
    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
