import React from 'react';

import HdgComponent from './HdgComponent';

export default {
    title: 'Heading Component',
    component: HdgComponent,
};

const Template = (args) => <HdgComponent {...args} />;

export const Large = Template.bind({});
Large.args = {
    heading_degrees: 15,
    rotate_frame: false,
    requested_azimuth_power_percentage: 30,
    sml: 'l',
};

export const LargeRotateFrame = Template.bind({});
LargeRotateFrame.args = {
    heading_degrees: 15,
    rotate_frame: true,
    requested_azimuth_power_percentage: 30,
    sml: 'l',
};

export const Medium = Template.bind({});
Medium.args = {
    heading_degrees: 15,
    rotate_frame: false,
    requested_azimuth_power_percentage: 30,
    sml: 'l',
};

export const Small = Template.bind({});
Small.args = {
    heading_degrees: 15,
    rotate_frame: false,
    requested_azimuth_power_percentage: 30,
    sml: 'l',
};
