import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AzimuthWatchFaceSvgComponent from './svg-components/AzimuthWatchFaceSvgComponent.jsx';
import MainEngineComponent from './MainEngineComponent.jsx';
import ArrowSvgComponent from './svg-components/ArrowSvgComponent.jsx';
export default class AzimuthComponent extends Component {
    render() {
        const { requestedAzimuthAngle_degrees, azimuth_angle_degrees, requested_azimuth_power_percentage, azimuth_power_percentage, sml } = this.props;
        let total_width = 256;
        let total_height = 256;
        let circle_padding = 24;
        let arrow_x_padding = 120;
        let main_engine_y_padding = 7;
        if (sml === 's') {
            total_width = 80;
            total_height = 80;
            circle_padding = 8;
            arrow_x_padding = 38;
            main_engine_y_padding = 0;
        } else if (sml === 'l') {
            total_width = 512;
            total_height = 512;
            circle_padding = 48;
            arrow_x_padding = 240;
            main_engine_y_padding = 14;
        }
        const viewBox = '0 0 ' + total_width + ' ' + total_height;
        let requested_arrow_rotate_string =
            'rotate(' + requestedAzimuthAngle_degrees + ' ' + total_width / 2 + ' ' + total_height / 2 + ')';
        return (
            <svg x={0} y={0} width={total_width} height={total_height} viewBox={viewBox}>
                <AzimuthWatchFaceSvgComponent x={circle_padding} y={circle_padding} sml={sml} />
                <MainEngineComponent
                    x={0}
                    y={main_engine_y_padding}
                    requested_throttle={requested_azimuth_power_percentage}
                    engine_throttle={azimuth_power_percentage}
                    rotation_degrees={azimuth_angle_degrees}
                    sml={sml}
                />
                <g transform={requested_arrow_rotate_string}>
                    <ArrowSvgComponent x={arrow_x_padding} y={main_engine_y_padding} fillClassName={'blksail-input'} sml={sml} />
                </g>
            </svg>
        );
    }
}
AzimuthComponent.defaultProps = {};

AzimuthComponent.propTypes = {
    requestedAzimuthAngle_degrees: PropTypes.number,
    azimuth_angle_degrees: PropTypes.number,
    requested_azimuth_power_percentage: PropTypes.number,
    azimuth_power_percentage: PropTypes.number,
    sml: PropTypes.string,
    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
