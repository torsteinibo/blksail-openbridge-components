import React from 'react';

import MainEngineComponent from "./MainEngineComponent";

export default {
    title: 'Main Engine Component',
    component: MainEngineComponent,
};

const Template = (args) => <MainEngineComponent {...args} />;

export const Large = Template.bind({});
Large.args = {
    x: 0,
    y: 0,
    requested_throttle: 20,
    engine_throttle: 15,
    rotation_degrees: 0,
    sml: 'l',
};
