import React from 'react';

import AzimuthComponent from './AzimuthComponent';

export default {
    title: 'AzimuthComponent',
    component: AzimuthComponent,
};

const Template = (args) => <AzimuthComponent {...args} />;

export const Large = Template.bind({});
Large.args = {
    requestedAzimuthAngle_degrees: 15,
    azimuth_angle_degrees: 20,
    requested_azimuth_power_percentage: 30,
    azimuth_power_percentage: 25,
    sml: 'l',
};

export const Medium = Template.bind({});
Medium.args = {
    requestedAzimuthAngle_degrees: 15,
    azimuth_angle_degrees: 20,
    requested_azimuth_power_percentage: 30,
    azimuth_power_percentage: 25,
    sml: 'm',
};

export const Small = Template.bind({});
Small.args = {
    requestedAzimuthAngle_degrees: 15,
    azimuth_angle_degrees: 20,
    requested_azimuth_power_percentage: 30,
    azimuth_power_percentage: 25,
    sml: 's',
};
