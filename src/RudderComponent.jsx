import React, { Component } from 'react';
import PropTypes from 'prop-types';

import WatchFaceSvgComponent from './svg-components/WatchFaceSvgComponent.jsx';
import RudderArrowSvgComponent from './svg-components/RudderArrowSvgComponent.jsx';
import RudderInnerCircleSvgComponent from './svg-components/RudderInnerCircleSvgComponent.jsx';

export default class RudderComponent extends Component{
  render() {
    const { rudder_angle_degrees, requested_rudder_angle_degrees, maxAngleDegrees, sml } = this.props;
    let total_width = 208;
    let total_height = 208;
    let inner_circle_radius = 32;

    if (sml === 's') {
      total_width = 64;
      total_height = 64;
      inner_circle_radius = 12;
    } else if (sml === 'l') {
      total_width = 416;
      total_height = 416;
      inner_circle_radius = 48;
    }

    let viewBox = '0 0 ' + total_width + ' ' + total_height;

    return (
      <svg x={0} y={0} width={total_width} height={total_height} viewBox={viewBox}>
        <WatchFaceSvgComponent x={0} y={0} maxAngleDegrees={maxAngleDegrees} sml={sml} />
        <RudderArrowSvgComponent
          x={total_width / 2}
          y={total_height / 2}
          rotation_degrees={requested_rudder_angle_degrees}
          fillClassName={'blksail-input'}
          sml={sml}
        />
        <RudderInnerCircleSvgComponent x={total_width / 2} y={total_height / 2} radius={inner_circle_radius} />
        <RudderArrowSvgComponent
          x={total_width / 2}
          y={total_height / 2}
          rotation_degrees={rudder_angle_degrees}
          fillClassName={'blksail-dynamic'}
          sml={sml}
        />
      </svg>
    );
  }
}

RudderComponent.defaultProps = {};

RudderComponent.propTypes = {
    rudder_angle_degrees: PropTypes.number,
    requested_rudder_angle_degrees: PropTypes.number,
    maxAngleDegrees: PropTypes.number,
    sml: PropTypes.string,
    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
