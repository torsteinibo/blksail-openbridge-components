import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class InterBarTicksMainEngine extends Component {
  render() {
    if (this.props.sml === 's') {
      return '';
    } else if (this.props.sml === 'm') {
      return '';
    } else if (this.props.sml === 'l') {
      return (
        <svg
          x={this.props.x}
          y={this.props.y}
          width="80"
          height="384"
          viewBox="0 0 80 384"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M80 192L4.22597e-05 192" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 201.6L4.22597e-05 201.6" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 211.2L4.22597e-05 211.2" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 220.8L3.8445e-05 220.8" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 230.4L3.8445e-05 230.4" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 240L3.8445e-05 240" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 249.6L3.8445e-05 249.6" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 259.2L3.8445e-05 259.2" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 268.8L3.8445e-05 268.8" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 278.4L3.8445e-05 278.4" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 288L3.8445e-05 288" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 297.6L3.8445e-05 297.6" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 307.2L3.46303e-05 307.2" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 316.8L3.46303e-05 316.8" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 326.4L3.46303e-05 326.4" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 336L3.46303e-05 336" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 355.2L3.46303e-05 355.2" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 374.4L3.46303e-05 374.4" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 345.6L3.46303e-05 345.6" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 364.8L3.46303e-05 364.8" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 172.8L4.22597e-05 172.8" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 182.4L4.22597e-05 182.4" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 153.6L4.22597e-05 153.6" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 163.2L4.22597e-05 163.2" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 134.4L4.22597e-05 134.4" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 144L4.22597e-05 144" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 115.2L4.60744e-05 115.2" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 124.8L4.60744e-05 124.8" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 96L4.60744e-05 96" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 105.6L4.60744e-05 105.6" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 76.8L4.60744e-05 76.8" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 57.6L4.60744e-05 57.6" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 38.4L4.98891e-05 38.4" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 19.2L4.98891e-05 19.2" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 9.6L4.98891e-05 9.6" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 28.8L4.98891e-05 28.8" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 48L4.60744e-05 48" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 67.2L4.60744e-05 67.2" className="blksail-border-divider-color" strokeOpacity="0.12" />
          <path d="M80 86.4L4.60744e-05 86.4" className="blksail-border-divider-color" strokeOpacity="0.12" />
        </svg>
      );
    } else {
      return '';
    }
  }
}

InterBarTicksMainEngine.defaultProps = {};

InterBarTicksMainEngine.propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    sml: PropTypes.string,
    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
