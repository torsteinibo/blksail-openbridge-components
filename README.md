OpenBridge Maritime Components by [blkSAIL](https://blksail.xyz)
=========================================

This set of OpenBridge components were developed at blkSAIL and were open-sourced under the following [license](LICENSE.md).
[Demo](https://torsteinibo.gitlab.io/blksail-openbridge-components/storybook/)

## Contributing
Feel free to contribute back to the project by reporting bugs and create pull requests for new code.

## References:
- [OpenBridge Design Guidelines](https://openbridge-ds.webflow.io/)
- [OpenBridge Figma Maritime Components](https://www.figma.com/file/hXqKWCNZMhpzRocjGFPyJ3/Maritime-Components-RELEASE)
